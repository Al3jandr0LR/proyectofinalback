var express = require('express'),
    bodyParser = require("body-parser"),
    connectionDatabase = require('./connection-database'),
    bcrypt = require('bcrypt'),
    sendMail = require('./send-mail'),
    pdf = require('html-pdf'),
    pdfHtml = require('./pdfHtml'),
    glob = require("glob"),
    app = express(),
    port = process.env.PORT || 3000;


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));


app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type,Accept");
    next();
});

app.listen(port);

console.log('listening on port: ' + port);


app.get('/loginUser', async function (req, res) {

    const data = {
        mail: req.query.mail,
        password: req.query.password
    }

    const result = await connectionDatabase.loginUser(data);

    if (result !== "" && bcrypt.compareSync(data.password, result.password)) {
        res.send(result);
    } else {
        res.send();
    }

});


app.post('/registerClient', async function (req, res) {
    const saltRounds = 10;
    const hash = bcrypt.hashSync(req.body.password, saltRounds);

    var data = {
        name: req.body.name,
        lastName: req.body.lastName,
        mail: req.body.mail,
        bankAccount: req.body.bankAccount,
        typeDebitCard: req.body.typeDebitCard,
        debitCard: req.body.debitCard,
        password: hash,
        addresses: {
            neighborhood: req.body.addresses.neighborhood,
            city: req.body.addresses.city,
            state: req.body.addresses.state,
            country: req.body.addresses.country
        }
    };

    

    const existClient = await connectionDatabase.loginUser(data);

    if (existClient === "") {
        const result = await connectionDatabase.registerClient(data);

        if (result.insertedId !== "") {
            sendMail.sendEmail(data.name, data.mail);
            res.send({
                result: "OK"
            });
        }
    } else {
        res.send();
    }

});



app.get('/movementClient', async function (req, res) {

    const data = {
        idClient: req.query.idClient,
        bankAccount: req.query.bankAccount,
        movement: req.query.movement,
        startDate: req.query.startDate,
        endDate: req.query.endDate
    }
   
    const result = await connectionDatabase.movementClient(data);
    

    res.send(result);

});


app.post('/savingAmountShop', async function (req, res) {
    const data = {
        idClient: req.query.idClient,
        amount: req.query.amount,
        reason: req.query.reason,
        videoPreview:req.query.videoPreview,  
        titlePreview:req.query.titlePreview,
        descriptionProductPreview:req.query.descriptionProductPreview 
    }

    const result = await connectionDatabase.savingAmountShop(data);
    res.send(result);
});





app.get('/informationZipCode', async function (req, res) {

    const data = {
        zipCode: req.query.zipCode
    }

    const result = await connectionDatabase.informationZipCode(data);

    res.send(result);


});


app.post('/savingAmount', async function (req, res) {

    var data = {
        idClient: req.body.idClient,
        ammount: req.body.ammount,
        type: req.body.type
    };

    const resultSavingAmmount = await connectionDatabase.savingAmount(data);


    res.send(resultSavingAmmount);

});

app.get('/downloadFile/:id', async function (req, res) {

    res.download("./pdfs/" + req.params.id);


});

app.get('/comprobante', async function (req, res) {


    const data = {
        idClient: req.query.idClient,
        name: req.query.name,
        ammount: req.query.ammount,
        day: req.query.day,
        month: req.query.month,
        year: req.query.year,
        hours: req.query.hours,
        minutes: req.query.minutes,
        seconds: req.query.seconds,
        numberCie: req.query.numberCie,
        reference: req.query.reference
    };

    var content;
    if (data.idClient.startsWith('A')) {
        content = pdfHtml.createHtmlPdfAhorro(data.numberCie,data.reference, data.ammount, data.day, data.month, data.year);
    } else {
        content = pdfHtml.createHtmlPdfInvertir(data.name, data.ammount, data.day, data.month, data.year);
    }
    const namePdf = "./pdfs/" + data.idClient + "-" + data.year + "" + data.month + "" + data.day + "_" + data.hours + "" + data.minutes + "" + data.seconds + ".pdf";

    pdf.create(content).toFile(namePdf, function (err, resp) {
        res.send({
            response: resp.filename
        });
    });

});

app.get('/listComprobante', async function (req, res) {
    const data = {
        idClient: req.query.idClient
    }
    glob("./pdfs/" + data.idClient + "*.pdf", function (er, files) {
        res.send(files);
    });
});


app.get('/accountClient', async function (req, res) {
    const data = {
        idClient: req.query.idClient
    }

    const result = await connectionDatabase.accountClient(data);
   
    res.send(result);

});





app.put('/changeMail', async function (req, res) {
    const data = {
        name: req.query.name,
        idClient: req.query.idClient,
        mail: req.query.mail
    }

   
    
    const resInfo = await connectionDatabase.accountClient(data);
    
    sendMail.sendEmailUpdateInformation(data.name, resInfo.mail);
    const result = await connectionDatabase.changeMail(data);
    res.send(result);
   
});

app.put('/changePassword', async function (req, res) {

    const saltRounds = 10;
    const hash = bcrypt.hashSync(req.query.password, saltRounds);

    const data = {
        name: req.query.name,
        idClient: req.query.idClient,
        password: hash
    }

 
   
    const resInfo = await connectionDatabase.accountClient(data);
    
    sendMail.sendEmailUpdateInformation(data.name, resInfo.email);
    const result = await connectionDatabase.changePassword(data);
    res.send(result);
   
   res.send("");
});

app.put('/changeDebitCard', async function (req, res) {
    const data = {
        name: req.query.name,
        idClient: req.query.idClient,
        debitCard: req.query.debitCard
    }
   

    const resInfo = await connectionDatabase.accountClient(data);
    
    sendMail.sendEmailUpdateInformation(data.name, resInfo.email);
    const result = await connectionDatabase.changeDebitCard(data);
    res.send(result);
   
});


app.delete('/deleteAccount', async function (req, res) {
    const data = {
        name: req.query.name,
        idClient: req.query.idClient
    }
    
  
    const resInfo = await connectionDatabase.accountClient(data);
    
    sendMail.sendEmailDeleteInformation(data.name, resInfo.email);
    const result = await connectionDatabase.deleteAccount(data);
    res.send(result);
 
   
});
