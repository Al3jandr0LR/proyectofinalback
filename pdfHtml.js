exports.createHtmlPdfAhorro = function (numberCie,reference,ammount, day, mounth, year){
    const html= `
    <!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet">
        <style>
            *{
                font-family: 'Ubuntu', sans-serif;
                box-sizing: border-box;
            }
            html,
            body {
                display: flex;
                height: 100%;
                background-color: #E5E5E5;
                flex-direction: column;
                justify-content: space-between;
                padding: 30px;
            }
            h2{
                color: #1fb0e0;
            }
            h2,p{
                text-align: center;
            }
        </style>
    </head>
    
    <body>
    
        <div>
            <img src="https://i.pinimg.com/originals/8a/bb/a7/8abba7beda38e99bd043e19f7eb728c9.png"
                style="display: block; width: 150px;" alt="">
            <p style="float: right;">México D.F. a ${day} de ${mounth} del ${year}</p>
        </div>
    
       
    
        <div style="margin: 40px 0; background-color: #EEEEEE; border-radius: 6px;">
            <br>
        <h2>Número de convenio CIE</h2>
        <br>
            <p>${numberCie}</p>
            <h2>Referencia</h2>
            <p>${reference}</p>
            <h2>Monto</h2>
            <p>$${ammount}</p>
        </div>
    
        <div>
            <h4><p>Podras pagar en cualquier sucursal BBVA y veras reflejado tu pago en 24 horas</p></h4>
            
        </div>
    
    </body>
    
    </html>
`;

return html;
};


exports.createHtmlPdfInvertir = function (name,ammount, day, mounth, year){
    const html= `
    <!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <style>
            *{
                box-sizing: border-box;
            }
            html,
            body {
                height: 100%;
                background-color: #F5F5F5;
                padding: 30px;
            }
        </style>
    </head>
    
    <body>
        <div>
            <img src="https://i.pinimg.com/originals/8a/bb/a7/8abba7beda38e99bd043e19f7eb728c9.png"
                style="display: block; width: 150px;" alt="">
            <p style="float: right;">México D.F. a ${day} de ${mounth} del ${year}</p>
        </div>
    
    
        <div style="margin: 150px 0;">
            <h2>A quien corresponda</h2>
            <p>PRESENTE</p>
            <p>Por medio de este presente hacemos de su conocimiento que el usuaio ${name}  realizo un
                movimiento de inversion.</p>
            <p>Asi mismo le informamos que el monto de la inversion fue de $${ammount}.00.</p>
        </div>
    
        <div>
            <h4>Atentamente</h4>
            <img src="./signature.png" style="display: block; width: 200px; border-bottom: #000000 solid 1px;" alt="">
        </div>
    </body>
    
    </html>
`;

return html;
};