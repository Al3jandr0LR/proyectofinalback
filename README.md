﻿# Proyecto Final Back Tech U 2020


1) Clonar proyecto
 
```bash
git clone https://Al3jandr0LR@bitbucket.org/Al3jandr0LR/proyectofinalback.git
```

2) Entrar a la carpeta "proyectofinalback"
 
```bash
cd proyectofinalback
```

3) Descargar dependencias expressjs
 
```bash
npm install
```

4) Construir proyecto dockerizado
 
```bash
docker build -t proyectofinalback .
```

5) Correr proyecto dockerizado
 
```bash
docker run -d -p 3002:3000 --name proyectofinalback proyectofinalback
```

6) Todos los endpoints estaran en el puerto 3002
 
[http://localhost:3002/](http://localhost:3002/)


