const requestjson = require('request-json');
const urlClientes = "https://webhooks.mongodb-stitch.com/api/client/v2.0/app/proyecto-qedtu/service/Proyecto/incoming_webhook/";
const clienteMLab = requestjson.createClient(urlClientes);
const urlClientes2 = "https://api-sepomex.hckdrk.mx/query/info_cp/";
const clienteSepoMex = requestjson.createClient(urlClientes2);


exports.loginUser = function (data) {
const url = "Clients?mail="+data.mail;
return clienteMLab.get(url).then(function (result) {
    return result.body;
  });
};


exports.registerClient = function (data) {

  return clienteMLab.post('RegisterClient', data).then(function (result) {
    return result.body;
});

};

exports.movementClient = function (data) { 
  const url = "MovementClient?idClient="+data.idClient+"&startDate="+data.startDate+"&endDate="+data.endDate+"&movement="+data.movement;
  
  return clienteMLab.get(url).then(function (result) {
    return result.body;
  });

  };


   exports.savingAmountShop = function (data) {
  
    return clienteMLab.post('savingAmountShop', data).then(function (result) {
      return result.body;
    });

   };



   exports.changeMail = function (data) {
  
    return clienteMLab.put('ChangeMail', data).then(function (result) {
     return result.body;
   });
   

   };


   exports.changePassword = function (data) {
  
    return clienteMLab.put('ChangePassword', data).then(function (result) {
     return result.body;
   });
   
   };

   exports.changeDebitCard = function (data) {
  
    return clienteMLab.put('ChangeDebitCard', data).then(function (result) {
     return result.body;
   });
   
   };


   exports.deleteAccount = function (data) {
    const url ="DeleteAccount?idClient="+data.idClient;
    
    return clienteMLab.del(url).then(function (result) {
      return result.body;
    });
   
   
   };


   exports.informationZipCode = function (data) {
    const r = clienteSepoMex.get(data.zipCode).then(function (result) {
      return result.body;
    });
    
   return r;
   };


   exports.savingAmount = function (data) {
    return clienteMLab.post('SavingAmount', data).then(function (result) {
      return result.body;
    });
   };


   exports.accountClient = function (data) {

    const url = "AccountClient?idClient="+ data.idClient;

    return clienteMLab.get(url).then(function (result) {
      
      return result.body;
    });

  
  }