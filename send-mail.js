var env = require('node-env-file'); // .env file
env(__dirname + '/.env.dist');

exports.sendEmail = function (userName, userMail) {

    const mailjet = require('node-mailjet').connect(
        process.env.MJ_APIKEY_PUBLIC,
        process.env.MJ_APIKEY_PRIVATE
    )
    const request = mailjet.post('send', {
        version: 'v3.1'
    }).request({
        Messages: [{
            From: {
                Email: 'alextestdev00@outlook.com',
                Name: 'BBVA',
            },
            To: [{
                Email: userMail,
                Name: userName,
            }, ],
            Subject: '¡Bienvenido!',
            HTMLPart: `
         
            <div
            style="width: 90%; padding: 24px; background-color: #002E64; border-radius: 4px; color: #ffffff; box-shadow: 5px 10px 8px #888888; margin: auto;">
            <img src="https://i.pinimg.com/originals/86/78/c8/8678c888099c3acdc6193c220413a31a.png" alt="bbvaLogoMail" style="width: 100%; display: block;">
            <h2 style="font-size: 40px; text-align: center; font-family: 'Leckerli One', cursive;">Bienvenido, ${userName}
            </h2>
            <p style="color:white ;font-size: 20px; font-family: 'Ubuntu', sans-serif;">Ahora eres parte de la empresa más grande de México que  tiene como objetivo ayudar a todos los sus clientes.
            </p>
            <p style=" color:white ; font-size: 20px; font-family: 'Ubuntu', sans-serif;">No te preocupes, nosotros te ayudamos en cada paso que das.</p>
            <a href="http://localhost:3000/"
                style="padding: 10px 30px; border-radius: 4px; background-color: #2FCCCD ; font-size: 20px; color: #ffffff; text-decoration: none; display:block; text-align: center; font-family: 'Ubuntu', sans-serif;">Acceder</a>
        </div>
      
               `
            
        }]
    })
    request
        .then(result => {
            console.log(result.body)
        })
        .catch(err => {
            console.log(err.statusCode)
        })

}





exports.sendEmailUpdateInformation = function (userName, userMail) {

    const mailjet = require('node-mailjet').connect(
        process.env.MJ_APIKEY_PUBLIC,
        process.env.MJ_APIKEY_PRIVATE
    )
    const request = mailjet.post('send', {
        version: 'v3.1'
    }).request({
        Messages: [{
            From: {
                Email: 'alextestdev00@outlook.com',
                Name: 'BBVA',
            },
            To: [{
                Email: userMail,
                Name: userName,
            }, ],
            Subject: '¡Actualización de información!',
            HTMLPart: `
         
            <div
            style="width: 90%; padding: 24px; background-color: #002E64; border-radius: 4px; color: #ffffff; box-shadow: 5px 10px 8px #888888; margin: auto;">
            <img src="https://i.pinimg.com/originals/86/78/c8/8678c888099c3acdc6193c220413a31a.png" alt="bbvaLogoMail" style="width: 100%; display: block;">
            <h2 style="font-size: 40px; text-align: center; font-family: 'Leckerli One', cursive;">Hola, ${userName}
            </h2>
            <p style="color:white ;font-size: 20px; font-family: 'Ubuntu', sans-serif;">Se a actualizado tu información
            </p>
            <p style="color:white ;font-size: 20px; font-family: 'Ubuntu', sans-serif;"></p>
            
        </div>
      
               `
            
        }]
    })
    request
        .then(result => {
            console.log(result.body)
        })
        .catch(err => {
            console.log(err.statusCode)
        })

}



exports.sendEmailDeleteInformation = function (userName, userMail) {

    const mailjet = require('node-mailjet').connect(
        process.env.MJ_APIKEY_PUBLIC,
        process.env.MJ_APIKEY_PRIVATE
    )
    const request = mailjet.post('send', {
        version: 'v3.1'
    }).request({
        Messages: [{
            From: {
                Email: 'alextestdev00@outlook.com',
                Name: 'BBVA',
            },
            To: [{
                Email: userMail,
                Name: userName,
            }, ],
            Subject: '¡Hasta pronto!',
            HTMLPart: `
         
            <div
            style="width: 90%; padding: 24px; background-color: #002E64; border-radius: 4px; color: #ffffff; box-shadow: 5px 10px 8px #888888; margin: auto;">
            <img src="https://i.pinimg.com/originals/86/78/c8/8678c888099c3acdc6193c220413a31a.png" alt="bbvaLogoMail" style="width: 100%; display: block;">
            <h2 style="font-size: 40px; text-align: center; font-family: 'Leckerli One', cursive;">Hasta pronto ${userName}
            </h2>
            <p style="font-size: 20px; font-family: 'Ubuntu', sans-serif;">Se a eliminado tu cuenta, pero no es un adios. Esperamos que vuelvas pronto.
            </p>
            <p style="font-size: 20px; font-family: 'Ubuntu', sans-serif;"></p>
            
        </div>
      
               `
            
        }]
    })
    request
        .then(result => {
            console.log(result.body)
        })
        .catch(err => {
            console.log(err.statusCode)
        })

}